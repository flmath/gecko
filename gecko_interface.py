import socket
import sys
import subprocess

HOST, PORT = "localhost", 9999
data = " ".join(sys.argv[1:])

def kill():
    subprocess.call("pkill geckodriver", shell=True)
    subprocess.call("pkill firefox", shell=True)
    subprocess.call("pkill python3", shell=True)
    print("geckodriver and firefox killed")
    
def null():
    print("NULL") 

# Create a socket (SOCK_STREAM means a TCP socket)
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:


    if len(sys.argv)<2: null()
    elif str(sys.argv[1]).upper()=='KILL': kill()
    else:
        # Connect to server and send data
        sock.connect((HOST, PORT))

        sock.sendall(bytes(data + "\n", "utf-8"))

        # Receive data from the server and shut down
        received = str(sock.recv(1024), "utf-8")
        print("Sent:     {}".format(data))
        print("Received: {}".format(received))

  
    
