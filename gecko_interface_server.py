import socketserver
import subprocess
import requests
import json

import os
import errno

from urllib.parse import urlunparse
from urllib.parse import urlparse
from datetime import datetime

pic_dir = '/home/pictures/'
port = 9999
firefox_headers = {'User-Agent': 'Mozilla/60.0'}
firefox_request_body = {"capabilities":{"alwaysMatch": {"acceptInsecureCerts": True}}}



##https://docs.python.org/3/library/socketserver.html#module-socketserver
class GeckoServerHandler(socketserver.BaseRequestHandler):
    """
    The intermediate node to gecko webdriver.
    """
    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        print("{} wrote:".format(self.client_address[0]))
        print(self.data)
        Command = self.data.upper()                      
        
        if Command=='START'.encode() : Status = self.start()
        elif Command=='STOP'.encode() :  Status = stop()
        #https://stackoverflow.com/questions/
        #17615414/how-to-convert-binary-string-to-normal-string-in-python3
        else:
              StrCommand = self.data.decode()
              UrlList=[url.strip() for url in StrCommand.split(sep=";")]
              Status = ""
              for Url in UrlList:
                  Status += self.handle_url(Url)
                  Status +="; "
        self.request.sendall(Status.encode())

    def start(self):
        returned_value = subprocess.call("firefox --headless --marionette &", shell=True)
        print('firefox started:', returned_value)
        returned_value = subprocess.call("geckodriver --connect-existing --marionette-port 2828 &",
                                         shell=True)
        print('geckodriver connected:', returned_value)
        
        self.server.session = requests.Session()
        print('firefox connecting..: ', firefox_headers, firefox_request_body)
        
        the_session = self.server.session
        print('session..: ', the_session)

        firefox_session = the_session.post('http://localhost:4444/session', headers=firefox_headers, json=firefox_request_body)
        print('firefox connected: ', firefox_session.status_code)
        firefox_session_json = json.loads(firefox_session.text)
        self.server.sessionId = firefox_session_json.get('value').get('sessionId')
        requestUri = 'http://localhost:4444/session/'
        requestUri += self.server.sessionId
        gotoUri =  requestUri+'/url'
        requestUri += '/moz/screenshot/full'
        print("screenshot request uri assembled ", requestUri)
        print("goto request uri assembled ",gotoUri)                
        self.server.requestUri = requestUri
        self.server.gotoUri = gotoUri
        print("STARTED sessionId "+self.server.sessionId)
        return ("STARTED sessionId "+self.server.sessionId)
                                                                      
    def handle_url(self, URL):
        if not self.server.sessionId:
            print("No connection to gecko established")
            return ("No connection to gecko established")
        (fixedUrl,netloc) = fix_url(URL)
        if not netloc:
             print("Invalid URL")
             return ("Invalid URL "+URL)
         
        pageUrl = {'url': fixedUrl}
        print("screenshot page: ", pageUrl)
        print("goto request uri used ", self.server.gotoUri)
        isPageSet = self.server.session.post(self.server.gotoUri, headers=firefox_headers, json=pageUrl)
        print("page is set ", isPageSet)
        print("screenshot request uri used ", self.server.requestUri)
        picture_response = self.server.session.get(self.server.requestUri, headers=firefox_headers, json=pageUrl)
        print("picture response received ", picture_response.status_code)
        picture_json = json.loads(picture_response.text)
        picture_base64 = picture_json.get('value')
        save(netloc, picture_base64)
        print("Handle "+fixedUrl+" file downloaded")
        return ("Handle "+fixedUrl+" file downloaded")        

def fix_url(URL):
    path=urlparse(URL)
    if path.netloc=='': return (None, None)
    if path.scheme=='': path._replace(scheme='http')
    return (urlunparse(path), path.netloc)
        
def save(path_str, picture): 
    path_list = path_str.split(sep=".")
    os.chdir(pic_dir)
    # https://stackoverflow.com/questions/
    #18973418/os-mkdirpath-returns-oserror-when-directory-does-not-exist           
    for path in path_list:
        try:
           os.mkdir(path)
        except OSError as exc:
           if exc.errno != errno.EEXIST:
            raise   
        os.chdir(path)

    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
    with open(timestamp, 'w') as file: file.write(picture)
    return "OK"
    
def stop():
    subprocess.call("pkill geckodriver", shell=True)
    subprocess.call("pkill firefox", shell=True)
    exit() 
    print("STOPPED")
    return "STOPPED"



if __name__ == "__main__":
    HOST, PORT = "localhost", port

    # Create the server, binding to localhost on port 9999
    with socketserver.TCPServer((HOST, PORT), GeckoServerHandler) as server:
        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        server.serve_forever()
