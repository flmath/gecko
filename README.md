# GECKO client


The application to create screenshots using the firefox web browser.

## Rationale: 

Rendering properly webpage become complicated in the world of an evolving 
HTML standards and dynamically loading javascript libraries.

Because of that, it is good to use one of the actively developed robust engines like Gecko(Firefox), Blink(Chrome) or Webkit(Safari).
I have decided to use Gecko.
The easiest way is to just use the command line: 

firefox -screenshot file.png https://developer.mozilla.com

Unfortunately, it runs full Firefox instance per to generate one picture.
For a more scalable solution, we need to start firefox, run the session, switch the URL to the correct one and make a screenshot.

## Solution:

Docker image contains minimal Ubuntu distribution, the python 3 (in which solution is coded), Firefox,gecko-driver and the python code.

There is no graphic environment which is resource consuming and unnecessary in an automated node.
The code consists of a server and client to communicate with it.

### gecko_interface_server.start
We start the server that will be connected by the client application (by default on the port 9999).

### gecko_interface_server.start

When we start the server we create a new Firefox instance configured to be manipulatable, then we start connected gecko driver which gives use interface to the Firefox node:
 
firefox --headless --marionette &

geckodriver --connect-existing --marionette-port 2828 &

We start firefox session and receive a sessionId which we need to store to keep the connection with the firefox
POST 'http://localhost:4444/session' ->sessionId
Then we store it and some data constructed on it in the server for quick access later.
### gecko_interface_server.handle
When we receive non-command data from client we are assuming that it is a string containing URLs to make a screenshot of. We assume that URLs are divided by semicolons ";". We split them and send to handle_url

### gecko_interface_server.handle_url


First, we check if we already set up a connection to Firefox, then we check if the URL is a valid network location. If yes, then we switch the current page of the firefox to the new one and make a screenshot.
We receive base64 encoded png image in response JSON which we store (I store it with the timestamp as the name in directory matching network location domains).
   
## Scaling:
Since we have a server inside the docker image we can run many instances with different ports and distribute inflow messages between them.
The output directory in the picture directory is constructed that we can merge it easily.
I also tried to use many firefox/marionette instances per gecko driver, but it requires the usage of Firefox profiles to change user_pref("marionette.defaultPrefs.port", 2829);
but I couldn't connect to profiled Firefox.

## Instructions:

host: cd gecko

host: docker build -t ubuntu:geck .

host: docker images 

host: docker run -it ubuntu:geck

root@ID: cd /home

root@ID:/home python3 gecko_interface_server.py &

root@ID:/home python3 gecko_interface.py start

root@ID:/home python3 gecko_interface.py "https://www.stackoverflow.com; www.google.com; http://www.google.com"


root@ID:/home python3 gecko_interface.py kill

host: rm -rf decoded/* pictures/*

host: docker cp ID:/home/pictures/ .

host: find pictures/  -type d | xargs -I % sh -c 'mkdir -p decoded/%'

host: find pictures/  -type f | xargs -I % sh -c 'echo %; echo %' | xargs -n2 -I % sh -c 'base64 -d  % > decoded/%.png'

## References:

https://github.com/mozilla/geckodriver/releases?after=v0.18.0

https://firefox-source-docs.mozilla.org/testing/geckodriver/Usage.html

https://github.com/mozilla/geckodriver

https://developer.mozilla.org/en-US/docs/Mozilla/Gecko/Embedding_Mozilla

https://developer.mozilla.org/en-US/docs/Mozilla/Gecko

https://docs.docker.com/engine/reference/commandline/cp/

https://docs.python.org/3.7/library/subprocess.html#module-subprocess

https://docs.python.org/3.8/library/socketserver.html
