FROM ubuntu:disco
MAINTAINER Mathias Green 

RUN apt-get update
RUN apt-get install -y firefox-geckodriver
RUN apt-get install -y python3-requests

COPY gecko_interface.py /home/
COPY gecko_interface_server.py /home/
RUN  mkdir -p /home/pictures